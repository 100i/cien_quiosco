CienQuiosco::Engine.routes.draw do
  # Select a page based upon the domain
  root to: 'page#index', constraints: { domain: 'testbench' }

  # Root off application
  root to: 'card#show'
end
