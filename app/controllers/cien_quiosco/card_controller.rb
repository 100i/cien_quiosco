require_dependency "cien_quiosco/application_controller"

module CienQuiosco
  class CardController < ApplicationController
    def index
    end

    def show
      @card = CienQuiosco::Card.find_by_domain(request.host)
    end
  end
end
