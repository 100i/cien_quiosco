require 'test_helper'

module CienQuiosco
  class CardControllerTest < ActionDispatch::IntegrationTest
    include Engine.routes.url_helpers

    test "should get index" do
      get card_index_url
      assert_response :success
    end

    test "should get show" do
      get card_show_url
      assert_response :success
    end

  end
end
