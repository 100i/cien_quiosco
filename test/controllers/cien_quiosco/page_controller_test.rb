require 'test_helper'

module CienQuiosco
  class PageControllerTest < ActionDispatch::IntegrationTest
    include Engine.routes.url_helpers

    test "should get index" do
      get page_index_url
      assert_response :success
    end

  end
end
