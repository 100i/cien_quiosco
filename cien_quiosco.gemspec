$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "cien_quiosco/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "cien_quiosco"
  s.version     = CienQuiosco::VERSION
  s.authors     = ["ekem"]
  s.email       = ["ekem@100.industries"]
  s.homepage    = "http://100.industries"
  s.summary     = "100.industries"
  s.description = "100.industries"
  s.license     = "MIT"

  s.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.md"]

  s.add_dependency "rails", "~> 5.0.1"
  s.add_dependency "material_design_lite-sass"

  s.add_development_dependency "sqlite3"
end
