class CreateCienQuioscoCards < ActiveRecord::Migration[5.0]
  def change
    create_table :cien_quiosco_cards do |t|
      t.text :domain
      t.text :description
      t.text :email

      t.timestamps
    end
  end
end
